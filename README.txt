Note: This module will not do much on its own and you will not be able to add items to your
card without programming in PHP. Also, this currently only works with US Amazon.com

INSTALL INSTRUCTIONS
---------------------
1. Upload the module to your modules directory
2. Enable the module at admin/build/modules
3. Enter your Amazon ECS key and associate tag at admin/settings/amazon_checkout

You will now be able to use the API to add items to your cart and modify it.
You can access your current Amazon remote cart at amazon_checkout/cart.
A new block should be available called "Amazon cart info block" at admin/build/block.

USING THE API
----------------------
Carts are saved in user sessions, so each visitor to your site will have their
own cart. Please note you must create your own pages or forms that call these
functions to add and remove items from the cart. The amazon_checkout/cart page
is included which will allow users to modify quantities of items in their cart
and checkout on Amazon.

function amazon_checkout_cart_add($asin, $quantity=1) {
Adds an item to the user's cart. $asin is the Amazon.com product ID.

function amazon_checkout_cart_get($full = FALSE) {
Gets information on the remote Amazon cart. If $full is false, this
only returns minimal data on the cart that has been cached locally
(subtotal, total # of items, etc). If $full is true, the whole 
Cart SimpleXML response object will be returned.

function amazon_checkout_cart_modify($items) {
Allows you to modify the quantity of any items in your cart. Use
this function to remove items by setting quantity to 0.

Look at the comments and first 3 functions of amazon_checkout.module
to get a more in depth description of the API functions.
